#!/usr/bin/env python3
import os, sys, random, time, os.path, shutil

# the variables
JAVAP = os.environ['JAVAP']
HERE = os.path.dirname(os.path.realpath(__file__))
JARDIR = f"{HERE}/../jars/"
VERJAR = sys.argv[1]
NICK = sys.argv[2]
MEM = sys.argv[3]
OS = sys.argv[4]
VVER = sys.argv[5]

MAIN_CLASS = open(f"{HERE}/../var/mainclass", "r").read()

PROXY_ENABLED = open(f"{HERE}/../var/enableproxy", "r").read()

def generate_classpath(CLASS_DIR):
    CP = ""
    for I in os.listdir(CLASS_DIR):
        CP = f"{CLASS_DIR}/{I}:{CP}"
    
    return CP

if PROXY_ENABLED == "True":
    PROXY_PATH = open(f"{HERE}/../var/proxy", "r").read()
    PROXYC = f"-Dhttp.proxyHost={PROXY_PATH}"
else:
    PROXYC = ""

CLASSPATH_GN = generate_classpath(f"{HERE}/../jars")

GM_DIR = open(f"{HERE}/../var/game_dir", "r").read()

# run the game
os.system(f"{JAVAP} -Xmx{MEM} -Xmn{MEM} -XX:+UseConcMarkSweepGC -XX:+CMSIncrementalMode -XX:-UseAdaptiveSizePolicy -Djava.library.path={HERE}/../natives/{OS} -cp {CLASSPATH_GN}{VERJAR} {PROXYC} -Djava.util.Arrays.useLegacyMergeSort=true {MAIN_CLASS} {NICK} password --username {NICK} --version {VVER} --accessToken 12734 --gameDir {GM_DIR}")
