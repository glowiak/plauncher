#!/usr/bin/env python3
import wx, os, sys, random
sys.path.append('branding')
sys.path.append('lang')
import Application as branding
import defaultLanguage as lang

# global app properties
App_Name = branding.App_Name
App_Name_Small = branding.App_Name_Small
Version = branding.Version

class App():
    def __init__(self):
        super().__init__()
        self.app = wx.App()
        self.window = wx.Frame(None, title = f"{App_Name} (WX GUI) {lang.settings}", size = (370,350))
        self.panel = wx.Panel(self.window)
        label = wx.StaticText(self.panel, label = f"{lang.apply_changes}.", pos = (5,2))
        l = open("var/enableproxy", "r")
        self.enableProxy = wx.CheckBox(self.panel, label = f"{lang.proxy}", pos = (2, 20))
        if l.read() == "True":
            self.enableProxy.SetValue(True)
        if l.read() == "False":
            self.enableProxy.SetValue(False)
        l.close()
        self.proxyPath = wx.TextCtrl(self.panel, size = (180, 20), pos = (190, 20))
        lo = open("var/proxy", "r")
        self.proxyPath.SetValue(lo.read())
        lo.close()
        self.applyChanges = wx.Button(self.panel, label = f"{lang.apply}", size = (90, 20), pos = (280, 130))
        self.applyChanges.Bind(wx.EVT_BUTTON, self.applySettings)
        self.mcText = wx.StaticText(self.panel, label = f"{lang.main_class}:", pos = (5,44))
        lMc = open("var/mainclass", "r")
        self.mainClass = wx.TextCtrl(self.panel, size = (180, 20), pos = (190,43), value = lMc.read())
        lMc.close()
        self.tpText = wx.StaticText(self.panel, pos = (5, 63), label = f"{lang.tar_prov}:")
        lTp = open("var/tar", "r")
        self.tarProv = wx.TextCtrl(self.panel, size = (180, 20), pos = (190, 63), value = lTp.read())
        lTp.close()
        self.laText = wx.StaticText(self.panel, pos = (5,83), label = f"{lang.language}:")
        lLg = open("var/lang", "r")
        self.langSel = wx.TextCtrl(self.panel, size = (180, 20), pos = (190, 83), value = lLg.read())
        lLg.close()
        self.gdText = wx.StaticText(self.panel, pos = (5, 103), label = f"{lang.gamedir}:")
        lgd = open("var/game_dir", "r")
        self.gameDir = wx.TextCtrl(self.panel, size = (180, 20), pos = (190, 103), value = lgd.read())
        lgd.close()
        # the logo
        img = wx.Image(1, 150)
        self.AppDispLogo = wx.StaticBitmap(self.panel, wx.ID_ANY, wx.Bitmap(img), pos = (1, 150))
        img2 = wx.Image('branding/logo.png', wx.BITMAP_TYPE_ANY)
        self.AppDispLogo.SetBitmap(wx.Bitmap(img2))
        self.window.Show(True)
        self.app.MainLoop()

    def applySettings(self, event):
        print(f"{lang.setDo}...")
        os.system("rm -f var/enableproxy var/proxy var/mainclass var/tar var/lang var/game_dir")
        sE = open("var/enableproxy", "w")
        sE.write(str(self.enableProxy.GetValue()))
        sE.close()
        sP = open("var/proxy", "w")
        sP.write(self.proxyPath.GetValue())
        sP.close()
        sMc = open("var/mainclass", "w")
        sMc.write(self.mainClass.GetValue())
        sMc.close()
        sTp = open("var/tar", "w")
        sTp.write(self.tarProv.GetValue())
        sTp.close()
        sLg = open("var/lang", "w")
        sLg.write(self.langSel.GetValue())
        sLg.close()
        os.system(f"rm -f lang/defaultLanguage.py && cp lang/{self.langSel.GetValue()}.py lang/defaultLanguage.py")
        sgd = open("var/game_dir", "w")
        sgd.write(self.gameDir.GetValue())
        sgd.close()
        print(f"{lang.done}")
        sys.exit(0)

if __name__ == '__main__':
    ex = App()
