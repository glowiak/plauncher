#!/usr/bin/env python3
import os, sys, random, os.path, shutil, subprocess, requests

HERE = os.path.dirname(os.path.realpath(__file__))

# arguments
JAVA = os.environ['JAVA']
NICK = os.environ['NICK']
MEM = os.environ['MEM']
OS = os.environ['OS']
CHANNEL = str(os.environ['CHANNEL']).upper()
VERSION = os.environ['VERSION']
TARP = open(f"{HERE}/../var/tar", "r").read()

def getVersionJar(channel2, version2):
    return f"{HERE}/../versions/{channel2}/{version2}.jar"

def downloadFile(src, dst):
    print(f"Downloading {src}")
    rv = requests.get(src, allow_redirects = True)
    open(dst, "wb").write(rv.content)

if not os.path.exists(f"{HERE}/../jars/lwjgl-2.9.0.jar"):
    os.system(f"(cd {HERE}/../jars/ && wget -q --no-check-certificate --input-file=filelist.txt)")

if not os.path.exists(f"{HERE}/../natives/{OS}/liblwjgl.so"):
    print("Unpacking natives...")
    os.system(f"{TARP} -d {HERE}/../natives/{OS}/ {HERE}/../jars/lwjgl-platform-2.9.0-natives-{OS}.jar")
    os.system(f"{TARP} -d {HERE}/../natives/{OS}/ {HERE}/../jars/jinput-platform-2.0.5-natives-{OS}.jar")
    print("REMOVE META-INF")
    os.system(f"rm -rvf {HERE}/../natives/{OS}/META-INF")

E = subprocess.check_output(f"sh {HERE}/xml.sh B{VERSION}B {HERE}/../versions/{CHANNEL}/manifest.xml", shell = True).decode('utf-8')

print(getVersionJar(CHANNEL, VERSION))

if not os.path.exists(getVersionJar(CHANNEL, VERSION)):
    if E == "":
        print("version not present")
        sys.exit(1)
    else:
        print("Downloading version jar...")
        # downloadFile(E, f"{HERE}/../versions/{CHANNEL}/{VERSION}.jar")
        os.system(f"wget -q --no-check-certificate -O {HERE}/../versions/{CHANNEL}/{VERSION}.jar $(sh {HERE}/xml.sh B{VERSION}B {HERE}/../versions/{CHANNEL}/manifest.xml)")

# launch the game
os.system(f"env JAVAP={JAVA} python3 {HERE}/runjar.py {getVersionJar(CHANNEL, VERSION)} {NICK} {MEM} {OS} {VERSION}")
