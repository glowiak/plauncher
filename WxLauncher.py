#!/usr/bin/env python3
import wx, os, sys, random
from PyQt5.QtWidgets import QFileDialog
sys.path.append('branding')
sys.path.append('lang')
import Application as branding
import defaultLanguage as lang

# global app properties
App_Name = branding.App_Name
App_Name_Small = branding.App_Name_Small
Version = branding.Version

class App():
    def __init__(self):
        super().__init__()
        self.app = wx.App()
        self.window = wx.Frame(None, title = f"{App_Name} v{Version} (WX GUI)", size = (370,350))
        self.panel = wx.Panel(self.window)
        self.label = wx.StaticText(self.panel, label = f"{lang.welcome} {App_Name}WX!", pos = (10,10))
        # load settings
        javPatt = open("var/java", "r")
        propJava = javPatt.read()
        javPatt.close()
        nickPatt = open("var/nick", "r")
        propNick = nickPatt.read()
        nickPatt.close()
        memPatt = open("var/mem", "r")
        propMem = memPatt.read()
        memPatt.close()
        osPatt = open("var/os", "r")
        propOS = osPatt.read()
        osPatt.close()
        channPatt = open("var/channel", "r")
        propChannel = channPatt.read()
        channPatt.close()
        verPatt = open("var/ver", "r")
        propVersion = verPatt.read()
        verPatt.close()
        # options
        self.jpText = wx.StaticText(self.panel, label = f"{lang.java_path}:", pos = (5,30))
        self.javaPath = wx.TextCtrl(self.panel, size = (195,20), pos = (70,30), value = propJava)
        self.nText = wx.StaticText(self.panel, label = f"{lang.nick}:", pos = (5,50))
        self.nick = wx.TextCtrl(self.panel, size = (100, 20), pos = (70, 50), value = propNick)
        self.mText = wx.StaticText(self.panel, label = f"{lang.memory}:", pos = (5,70))
        self.mem = wx.TextCtrl(self.panel, size = (100, 20), pos = (70, 70), value = propMem)
        self.oText = wx.StaticText(self.panel, label = f"{lang.os}:", pos = (5, 90))
        self.osys = wx.TextCtrl(self.panel, size = (100, 20), pos = (70, 90), value = propOS)
        self.cText = wx.StaticText(self.panel, label = f"{lang.channel}:", pos = (5, 110))
        self.channel = wx.TextCtrl(self.panel, size = (100,20), pos = (70, 110), value = propChannel)
        self.vText = wx.StaticText(self.panel, label = f"{lang.version}:", pos = (5, 130))
        self.ver = wx.TextCtrl(self.panel, size = (100, 20), pos = (70, 130), value = propVersion)
        # buttons
        self.forgeButt = wx.Button(self.panel, pos = (200, 95), size = (100, 20), label = lang.instForge)
        self.forgeButt.Bind(wx.EVT_BUTTON, self.installForge)
        self.PlayButt = wx.Button(self.panel, pos = (200, 115), size = (100, 20), label = lang.play)
        self.PlayButt.Bind(wx.EVT_BUTTON, self.runGame)
        self.WebButt = wx.Button(self.panel, pos = (200, 55), size = (160, 20), label = f"{App_Name} {lang.website}")
        self.WebButt.Bind(wx.EVT_BUTTON, self.website)
        self.SaveButt = wx.Button(self.panel, pos = (200, 75), size = (100, 20), label = lang.saveSett)
        self.SaveButt.Bind(wx.EVT_BUTTON, self.saveSetts)
        self.AdvButt = wx.Button(self.panel, pos = (270, 0), size = (100, 20), label = lang.advanced)
        self.AdvButt.Bind(wx.EVT_BUTTON, self.advancedOpts)
        self.modButt = wx.Button(self.panel, pos = (270, 20), size = (100, 20), label = lang.addJar)
        self.modButt.Bind(wx.EVT_BUTTON, self.addToJar)
        self.window.Show(True)
        # the logo
        img = wx.Image(1, 150)
        self.AppDispLogo = wx.StaticBitmap(self.panel, wx.ID_ANY, wx.Bitmap(img), pos = (1, 150))
        img2 = wx.Image('branding/logo.png', wx.BITMAP_TYPE_ANY)
        self.AppDispLogo.SetBitmap(wx.Bitmap(img2))
        self.app.MainLoop()

    def installForge(self, event):
        os.system(f"sh lib/forgeinst.sh {self.channel.GetValue()} {self.ver.GetValue()}")

    def runGame(self, event):
        rjava = self.javaPath.GetValue()
        rnick = self.nick.GetValue()
        rmem = self.mem.GetValue()
        ros = self.osys.GetValue()
        rchannel = self.channel.GetValue()
        rver = self.ver.GetValue()
        print(f"{lang.java_path}: ", rjava)
        print(f"{lang.nick}: ", rnick)
        print(f"{lang.memory}: ", rmem)
        print(f"{lang.os}: ", ros)
        print(f"{lang.channel}: ", rchannel)
        print(f"{lang.version}: ", rver)
        rString = (f"env JAVA={rjava} NICK={rnick} MEM={rmem} OS={ros} CHANNEL={rchannel} VERSION={rver} sh lib/cli.sh")
        print(f"{lang.cmd_to_be_exec}: {rString}")
        os.system(rString)

    def website(self, event):
        os.system(f"/usr/bin/env xdg-open {branding.Website}")

    def saveSetts(self, event):
        print(f"{lang.setDo}...")
        os.system("rm -f var/java var/nick var/mem var/os var/channel var/ver")
        javPat = open("var/java", "w")
        javPat.write(self.javaPath.GetValue())
        javPat.close()
        nick1 = open("var/nick", "w")
        nick1.write(self.nick.GetValue())
        nick1.close()
        mem1 = open("var/mem", "w")
        mem1.write(self.mem.GetValue())
        mem1.close()
        osys1 = open("var/os", "w")
        osys1.write(self.osys.GetValue())
        osys1.close()
        chann = open("var/channel", "w")
        chann.write(self.channel.GetValue())
        chann.close()
        verr = open("var/ver", "w")
        verr.write(self.ver.GetValue())
        verr.close()
        print("Done")


    def advancedOpts(self, event):
        os.system("/usr/bin/env python3 WxSettings.py")

    def addToJar(self, event):
        print("test")
        try:
            self.getModZip = wx.FileDialog(self.window, lang.openFile, os.getcwd(), "", f"{lang.modFiles} (*.zip *.jar)|*.zip *.jar", wx.FD_OPEN)
        #self.getModZip = wx.FileDialog(self.window, lang.openFile, "", "", f"{lang.modFiles} (*.zip *.jar)|*.zip *.jar", wx.FD_OPEN | wx.FD_FILE_MUST_EXIST)
        except:
            print("Unable to open file")
        
        #os.system(f"sh lib/add_mod.sh {self.channel.GetValue()} {self.ver.GetValue()} {self.getModZip.GetPath()}")
    
if __name__ == '__main__': 
    ex = App()
