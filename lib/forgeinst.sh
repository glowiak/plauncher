#!/bin/sh
# Forge installer for PLauncher
# usage: forgeinst.sh channel release

CHANNEL=$1
RELEASE=$2
HERE=$(readlink -f $(dirname $0))
TMP=/tmp/forgeinst${CHANNEL}${RELEASE}
FORGEDIR=$HERE/../forge

rm -rf $TMP
mkdir -p $TMP

CHANNEL=$(echo $CHANNEL | tr [a-z] [A-z])

# return error if version is missing
if [ ! -f "$HERE/../versions/$CHANNEL/$RELEASE.jar" ]
then
    $HERE/qinfo.py "Version_does_not_exist.Download_it_first!"
    exit 1
fi

# download forge
wget --no-check-certificate -O $FORGEDIR/${CHANNEL}-${RELEASE}-forge.zip $(sh $HERE/xml.sh B${RELEASE}B $FORGEDIR/${CHANNEL}.xml)


# create the jarmods directory if not empty
mkdir -p $HERE/../versions/${CHANNEL}/${RELEASE}-jarmods/

# copy forge file to the jarmods directory
cp -fv $FORGEDIR/${CHANNEL}-${RELEASE}-forge.zip $HERE/../versions/${CHANNEL}/${RELEASE}-jarmods/
