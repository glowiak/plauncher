#!/usr/bin/env python3
import os, sys, random, os.path, shutil, subprocess, requests

HERE = os.path.dirname(os.path.realpath(__file__))

# arguments
JAVA = open(f"{HERE}/../var/java", "r").read()
NICK = open(f"{HERE}/../var/nick", "r").read()
MEM = open(f"{HERE}/../var/mem", "r").read()
OS = open(f"{HERE}/../var/os", "r").read()

VJAR = os.environ['VJAR']
VERSION = os.environ['VERSION']

TARP = open(f"{HERE}/../var/tar", "r").read()

def downloadFile(src, dst):
    print(f"Downloading {src}")
    rv = requests.get(src, allow_redirects = True)
    open(dst, "wb").write(rv.content)

if not os.path.exists(f"{HERE}/../jars/lwjgl-2.9.0.jar"):
    os.system(f"(cd {HERE}/../jars/ && wget -q --no-check-certificate --input-file=filelist.txt)")

if not os.path.exists(f"{HERE}/../natives/{OS}/liblwjgl.so"):
    print("Unpacking natives...")
    os.system(f"{TARP} -d {HERE}/../natives/{OS}/ {HERE}/../jars/lwjgl-platform-2.9.0-natives-{OS}.jar")
    os.system(f"{TARP} -d {HERE}/../natives/{OS}/ {HERE}/../jars/jinput-platform-2.0.5-natives-{OS}.jar")
    print("REMOVE META-INF")
    os.system(f"rm -rvf {HERE}/../natives/{OS}/META-INF")

# launch the game
os.system(f"env JAVAP={JAVA} python3 {HERE}/runjar.py /tmp/*modded*.jar {NICK} {MEM} {OS} {VERSION}")
